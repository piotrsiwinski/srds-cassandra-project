#Cassandra app 

To run app:
```
./mvnw spring-boot:run
```

To run cqlsh:
```
docker exec -ti cas1 cqlsh
```

Run ```create-table.cql``` from src/main/resources/static to create schema

Run on specific port:
```
./mvnw spring-boot:run -Dserver.port=5000
```