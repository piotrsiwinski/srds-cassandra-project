package pl.pp.srds.cassandraproject.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;
import pl.pp.srds.cassandraproject.model.Product;

import java.util.List;

@Repository
public interface ProductRepository extends CassandraRepository<Product, String> {

  List<Product> getByProductName(String name);
}
