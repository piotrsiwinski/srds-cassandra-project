package pl.pp.srds.cassandraproject.config;

import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.QueryOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;
import org.springframework.data.cassandra.core.mapping.BasicCassandraMappingContext;
import org.springframework.data.cassandra.core.mapping.CassandraMappingContext;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;
import pl.pp.srds.cassandraproject.repository.ProductRepository;

@Configuration
@EnableCassandraRepositories(basePackageClasses = ProductRepository.class)
public class CassandraConfig extends AbstractCassandraConfiguration {

  @Value("${app.cassandra.cluster.contactPoints}")
  private String contactPoints;

  @Value("${app.cassandra.cluster.port}")
  private Integer port;

  @Value("${app.cassandra.keyspace}")
  private String keyspace;

  @Override
  protected String getKeyspaceName() {
    return keyspace;
  }

  @Bean
  public CassandraClusterFactoryBean cluster() {
    CassandraClusterFactoryBean cluster = new CassandraClusterFactoryBean();
    cluster.setContactPoints(contactPoints);
    cluster.setPort(port);
    cluster.setJmxReportingEnabled(false);
    cluster.setQueryOptions(new QueryOptions().setConsistencyLevel(ConsistencyLevel.ONE));
    return cluster;
  }

  @Bean
  public CassandraMappingContext cassandraMapping() {
    return new BasicCassandraMappingContext();
  }
}
