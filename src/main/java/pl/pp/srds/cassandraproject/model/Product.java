package pl.pp.srds.cassandraproject.model;

import lombok.Data;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.Instant;

@Table
@Data
public class Product {
  @PrimaryKeyColumn(name = "productName", ordinal = 2, type = PrimaryKeyType.PARTITIONED)
  private String productName;

  @Column private String description;

  @Column private String username;

  @Column private Double price;

  @Column private Instant startDate;

  @Column private Instant endDate;

  @Column private Instant bidDate;

  @Column private Boolean active;
}
