package pl.pp.srds.cassandraproject.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.pp.srds.cassandraproject.model.Product;
import pl.pp.srds.cassandraproject.repository.ProductRepository;

import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@RestController
@RequiredArgsConstructor
@Slf4j
public class ProductController {

  private final ProductRepository repository;

  @GetMapping("/products")
  public ResponseEntity<List<Product>> getAllProducts() {
    List<Product> all = repository.findAll();
    return ResponseEntity.ok(all);
  }

  private Optional<Product> getProd(List<Product> productsByName) {
    Optional<Product> productWithMaxPrice =
        productsByName.stream().max(Comparator.comparing(Product::getPrice));

    if (productWithMaxPrice.isPresent()) {
      Product product = productWithMaxPrice.get();
      log.info("Product: {} with max price {}", product.getProductName(), product.getPrice());

      if (isNull(product.getEndDate())) {
        Optional<Product> first =
            productsByName.stream().filter(e -> Objects.nonNull(e.getEndDate())).findFirst();
        first.ifPresent(product1 -> product.setEndDate(product1.getEndDate()));
      }

      if (isNull(product.getActive())) {
        Optional<Product> first =
            productsByName.stream().filter(e -> Objects.nonNull(e.getActive())).findFirst();
        first.ifPresent(product1 -> product.setActive(product1.getActive()));
      }

      List<Product> productsWithSamePrice =
          productsByName
              .stream()
              .filter(e -> e.getPrice().equals(product.getPrice()))
              .collect(Collectors.toList());
      if (productsWithSamePrice.size() > 1) {
        log.info(
            "Found {} bids with same price {} ", productsWithSamePrice.size(), product.getPrice());
        return productsWithSamePrice.stream().min(Comparator.comparing(Product::getBidDate));
      }
    }
    return productWithMaxPrice;
  }

  @GetMapping("product/{name}")
  public ResponseEntity<?> getProduct(@PathVariable String name) {
    List<Product> byProductName = repository.getByProductName(name);
    Optional<Product> prod = getProd(byProductName);
    return prod.map(ResponseEntity::ok).orElse(ResponseEntity.badRequest().build());
  }

  @PostMapping("/product")
  public ResponseEntity<?> createProduct(@RequestBody Product product) {

    if (StringUtils.isBlank(product.getUsername())) {
      return ResponseEntity.ok().body("Username is required");
    }

    List<Product> byProductName = repository.getByProductName(product.getProductName());
    Optional<Product> prod = getProd(byProductName);

    if (prod.isPresent()) {
      Product presentProduct = prod.get();

      if (Instant.from(presentProduct.getEndDate()).isBefore(Instant.now())
          || !presentProduct.getActive()) {
        log.info("Auction not active. Winner is: " + presentProduct.getUsername() + " and the price is: " + presentProduct.getPrice());
        return ResponseEntity.ok().body("Auction not active. Winner is: " + presentProduct.getUsername() + " and the price is: " + presentProduct.getPrice());
      }

      if (prod.get().getPrice().equals(product.getPrice())) {
        log.info("There is existing bid with the same price");
        return ResponseEntity.ok().body("There is existing bid with the same price");
      }
    }
    product.setBidDate(Instant.now());
    Product save = repository.save(product);
    return ResponseEntity.ok(save);
  }

  @PostMapping("/close-auction")
  public ResponseEntity<String> closeAuction(@RequestBody Product productName) {
    if (isNull(productName.getProductName())) {
      return ResponseEntity.ok().body("There is no auction with given name");
    }
    List<Product> byProductName = repository.getByProductName(productName.getProductName());

    if (byProductName.size() == 0) {
      log.info("There is no auction with given name");
      return ResponseEntity.ok().body("There is no auction with given name");
    }

    byProductName.forEach(
        product -> {
          product.setActive(false);
          product.setEndDate(Instant.now());
        });

    repository.saveAll(byProductName);
    log.info("Auction finished successfully");

    Optional<Product> winningOffer = getProd(byProductName);

    return winningOffer
        .map(
            product ->
                ResponseEntity.ok()
                    .body(
                        String.format(
                            "Auction %s finished successfully. And the winner is: %s",
                            product.getProductName(), product.getUsername())))
        .orElseGet(() -> ResponseEntity.ok().body("There is no winning offer"));
  }
}
