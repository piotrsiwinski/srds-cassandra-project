package pl.pp.srds.cassandraproject.repository;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraAdminOperations;
import org.springframework.data.cassandra.core.cql.CqlIdentifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.pp.srds.cassandraproject.config.CassandraConfig;
import pl.pp.srds.cassandraproject.model.Product;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CassandraConfig.class})
@Ignore("Not working - problems with connecting to EmbeddedCassandra")
public class ProductRepositoryIntegrationTest {

  private static final String DATA_TABLE_NAME = "product";
  @Autowired private ProductRepository productRepository;
  @Autowired private CassandraAdminOperations adminTemplate;

  @BeforeClass
  public static void startCassandraEmbedded() throws Exception {
    EmbeddedCassandraServerHelper.startEmbeddedCassandra();
    Cluster cluster =
        Cluster.builder()
            .addContactPoints("127.0.0.1")
            .withPort(9142)
            .withoutJMXReporting()
            .build();
    Session session = cluster.connect();
  }

  @AfterClass
  public static void stopCassandraEmbedded() {
    EmbeddedCassandraServerHelper.cleanEmbeddedCassandra();
  }

  //
  @Before
  public void createTable() {
    Map<String, Object> props = new HashMap<>();
    props.put("keyspace", "testkeyspace");

    adminTemplate.createTable(
        true, CqlIdentifier.of(DATA_TABLE_NAME), Product.class, new HashMap<>());
  }

  @Test
  public void should_create_new_product() {
    Product product = new Product();
    product.setProductName("test-product-name");
    product.setDescription("test-product-description");
    productRepository.save(product);

    List<Product> all = productRepository.findAll();
    //    assertTrue(byId.isPresent());
    //    assertEquals(byId.get().getProductName(), "testproductname");
  }

  @After
  public void dropTable() {
    adminTemplate.dropTable(CqlIdentifier.of(DATA_TABLE_NAME));
  }
}
