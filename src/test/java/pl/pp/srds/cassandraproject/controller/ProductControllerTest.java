package pl.pp.srds.cassandraproject.controller;

import org.junit.Test;
import org.springframework.web.client.RestTemplate;
import pl.pp.srds.cassandraproject.model.Product;

import java.util.HashMap;
import java.util.Map;

public class ProductControllerTest {
  private static final String BASE_API_URL = "http://localhost:8080";
  private static final String BASE_API_URL_WITHOUT_PORT = "http://localhost";
  private String[] API_URLS = {"http://150.254.32.154:8080","http://150.254.32.155:8080","http://150.254.32.156:8080"};
  private static final int THREADS_NUMBER = 10;

  @Test
  public void whenRequestingManyTimesFinishAuctionWithCorrectWinner() throws InterruptedException {
    String productName = "laptop";
    RestTemplate restTemplate = new RestTemplate();
    int randomNumber = 700;
    Map<Integer, Thread > threadMap = new HashMap<>();


    for (int k = 0; k < 3; k++) {
      int addr = k;
      Thread t =
          new Thread(
              () -> {
                String apiUrl = API_URLS[addr];
                System.out.println("Address: " + apiUrl);
                for (int i = 10; i < 1000; i++) {
                  Product product = new Product();
                  product.setProductName(productName);
                  product.setUsername("Mati " + i + Thread.currentThread().getName());
                  product.setPrice((double) i);
                  System.out.println("PODBIJAM OFERTE: " + product.getUsername());
                  System.out.println(
                      "SENDING POST REQUEST TO "
                          + apiUrl
                          + " "
                          + Thread.currentThread().getName());

                  String postForProduct = apiUrl + "/product";
                  System.out.println(postForProduct);
                  String result =
                      restTemplate.postForObject(postForProduct, product, String.class);


                  System.out.println(result + Thread.currentThread().getName());
                  System.out.println(
                      String.format("%s: %s", product.getUsername(), product.getPrice()));
                  if (i == randomNumber && apiUrl.equals("http://150.254.32.154:8080")) {
                    Product p = new Product();
                    p.setProductName(productName);
                    restTemplate.postForObject(apiUrl + "/close-auction", p, String.class);
                  }

                  if(result != null && result.startsWith("Auction not active")){
                    break;
                  }

                }
              });
      t.setName("HOST: " + addr);
      t.start();
      threadMap.put(addr, t);
    }

    threadMap.values().forEach(t -> {
      try {
        t.join();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    });
  }
}

//  Product product = new Product();
//    for (int i = 10; i < 1000; i++) {
//    product.setProductName(productName);
//    product.setUsername("Mati " + i);
//    product.setPrice((double) i);
//
//    for (int j = 5000; j < 5003; j++) {
//    Thread t = new Thread(() -> {
//
//    })
//    String result =
//    restTemplate
////                .postForObject(String.format("%s:%d/product", BASE_API_URL_WITHOUT_PORT, j),
// product, String.class);
//    .postForObject(BASE_API_URL + "/product", product, String.class);
//
//    System.out.println(result);
//    }
//
//    System.out.println(String.format("%s: %s", product.getUsername(), product.getPrice()));
//    if (i == randomNumber) {
//    Product p = new Product();
//    p.setProductName(productName);
//    restTemplate.postForObject(BASE_API_URL + "/close-auction", p, String.class);
//    }
//    }

//    Thread[] threads = new Thread[THREADS_NUMBER];

//    for (int i = 0; i < THREADS_NUMBER; i++) {
//      Thread t =
//          new Thread(
//              () -> {
//                RestTemplate restTemplate = new RestTemplate();
//                for (int j = 0; j < 100; j++) {
//                  Product product = new Product();
//                  product.setProductName(productName);
//                  product.setUsername("Mati " + j + Thread.currentThread().getName());
//                  product.setPrice((double) j);
//
//                  String result =
//                      restTemplate.postForObject(BASE_API_URL + "/product", product,
// String.class);
//                  System.out.println(
//                      String.format("%s: %s", product.getUsername(), product.getPrice()));
//                }
//              });
//      t.setName("THEAD - " + i);
//
//      threads[i] = t;
//    }
//
//    for (Thread t : threads) {
//      t.start();
//    }
//    for (Thread t : threads) {
//      t.join();
//    }
//
//    RestTemplate restTemplate = new RestTemplate();
//    String forObject =
//        restTemplate.getForObject(BASE_API_URL + "/product/" + productName, String.class);
//
//    System.out.println(forObject);
